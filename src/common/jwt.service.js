const ID_TOKEN_KEY = "id_token";

export const getToken = () => {
  return window.localStorage.getItem(ID_TOKEN_KEY);
};

export const getUser = () => {
  return window.localStorage.getItem("user");
}

export const setUser = (user) => {
  return window.localStorage.setItem("user", user)
}

export const destroyUser = () => {
  window.localStorage.removeItem("user");
}
export const saveToken = token => {
  window.localStorage.setItem(ID_TOKEN_KEY, token);
};

export const destroyToken = () => {
  window.localStorage.removeItem(ID_TOKEN_KEY);
};

export default { getToken, saveToken, destroyToken };
