import JwtService from "@/common/jwt.service";
import {LOGIN, CHECK_AUTH} from './actions.type'
import { SET_AUTH, PURGE_AUTH} from "./mutations.type";


const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  }
};


const state = {
  errors: null,
  user: {},
  isAuthenticated: false
};


const actions = {
  [LOGIN](context, credentials) {
    let user = {
      errors: null,
      user: credentials,
      isAuthenticated: true
    }
    context.commit(SET_AUTH, user );
  },
  [CHECK_AUTH](context) {
    if(!JwtService.getUser()){
      context.commit(PURGE_AUTH)
    }
  }
};


const mutations = {
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
    JwtService.setUser(user);
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    JwtService.destroyUser();
  }
};


export default {
  state,
  actions,
  getters,
  mutations
};
